# WormBiome #

This is the temporary readme file of the WormBiome package. This script is still in active development and more will come soon!

## Functional Database

The WormBiome database currently include 186 genomes of bacteria found associated with C. elegans in the wild. Those genomes have been annotated with PATRIC (prokka) and kofam and their metabolic potential has been reconstructed with gapseq. I am working on a way to have all the data available to download.

For now you can find our interactive annotations tables here:

- Metabolic database: - [Metacyc](https://airtable.com/shrde9W0dPShfkewK/tbla5PIVzz4e6gaAm)
- Functional database: - [Kegg](https://airtable.com/shrCX6wiLBu6AC7Ni)

Wormbiome is both a functional database of known bacterial genomes associated to C. elegans and a pipeline designed predict functional profile of defined microbial communities.

### Metacyc

To reconstruct the metabolic potential of each bacteria we process each genome thought the gapseq pipeline.

### Kegg

Each genome has been screen for Kegg annotation using Kofam.

### Full annotation

A full annotation databse, currently not included in the pipeline is available. This is a collection of annotations pulled from the PATRIC website.

## Pipeline
The pipeline processes result of 16S rRNA amplicon sequencing experiments performed with defined microbial communities and predict their metabolic profile.

# Installation

## Pre requirements

To use Wormbiome you will need to have:

- Local installation of BLAST
- R, with the following packages:
  - readr
  - tidyverse
  - reshape2
  - vegan
  - RcolorBrewer
  - labdsv
  - FSA
  - Phyloseq
  - DESeq2

## Conda

There is currently no conda package available for WormBiome. But we advise you to use conda to manually create a environment to run the pipeline

###  Create the environment

```
conda create --name wormbiome
conda activate wormbiome
conda config --add channels defaults && conda config --add channels bioconda && conda config --add channels conda-forge
```

### Install the tools

```
# basic dependencies
conda install bash r perl parallel gawk sed grep bc git coreutils wget

# install blast
conda install blast

# R-package dependencies (via conda repos)
conda install r-tidyverse r-readr r-reshape2 r-vegan r-RcolorBrewer r-labdsv r-FSA r-biocmanager bioconductor-phyloseq bioconductor-deseq2
```
Once you have those package installed, clone the git repository

```
git clone && cd wormbiome
```

# Run the Pipeline

## The files needed to run the pipeline

### Bacteria Collection

To run the pipeline you need to provide a file with the 16S rRNA sequences of the bacteria you used in your experiments.
Once you have this fasta file, place it in the `Data/Collection` folder.

Currently there are a few example in the `Data/Collection` folder, mainly the CeMbio and Big68 communities.

### ASV table

This table is your standard output of a 16S rRNA pipeline, such as [Qiime2](https://qiime2.org/), Mothur(https://mothur.org/) or directly from [Deblur](https://github.com/biocore/deblur) or [DaDa2](https://benjjneb.github.io/dada2/)

At the moment the Sample ID column needs to be named `X.SampleID`. I am working on making it smart.

### Representative ASV file

This is a fasta file with the representative sequences for the ASV name used in the ASV table above. It should be an output from your 16S rRNA pipeline.

### Metadata

At the moment the Sample ID column needs to be named `X.SampleID`. I am working on making it smart. Other than that it should be a tab delimited file with all the information you think is needed. To run the statistic module from the pipeline you will give the name of one of the column in your metadata file.

## Running the script

This part is still in active development, I hope to have all the core information updated here very soon!

```
~/Your/Path/wormbiom.sh -a ASV.table.tsv -m metadata.txt -s ASV.sequences.fasta -b BacteriaCollectionName -o Output_Folder -s1 TestGroup -v -st
```

`BacteriaCollectionName` correspond to the name of the Bacteria Collection file you create or use (see above)

## In details

Below is detailed explanation of what each option stands for

```
Options:
   -h|--help                Display this help

   -a|--ASVtable            Required. Biom/tsv/csv File

   -m|--ASVmeta             Required. Count table related Metadata

   -s|--ASVsequence         Required. related ASV sequences

   -b|--BacteriaList        Required. Collection Name or list of bacteria
                            name used in the experiment. One per line

   -o|--OutFolder           Output OutFolder
                            Default: ./OUT/

   -s1|--Selector1          Name of column of the metadata category that defines
                            your dataset. E.g: "Host Strain"

   -k|--kegg                Generate Kegg annotation table

   -p|--PathwayConvert      Generate Metacyc Metabolic Pathway Table.

   -pl|--PathwayTable       If Metabolic Pathway is already generated,
                            indicate the location of the file here
                            Incompatible with -p

   -kl|--keggTable          If Kegg annotation Table is already generated,
                            indicate the location of the file here
                            Incompatible with -k

   -bn|--BacteriaName       Bacteria Collection Name

   -st|--Stats              Run Deseq2 using the "selector1" as category to test

   -v|--Verbose             Verbose mode, the script will talk to you, a lot
```
