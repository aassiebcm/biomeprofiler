#!/bin/bash
# ------------------------------------------------------------------
# [Adrien Assié] WormBiom: Estimated profiles of C. elegans related
#               Microbiome based on genome and 16S rRNA Amplicons
# ------------------------------------------------------------------

# Last update 04/05/21 -- V0.3

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

VERSION=0.3
SUBJECT=WormBiom
USAGE="wormbiom.sh -a ASV-TABLE -m ASV-METADATA -s ASV-FASTA -b BACTERIALIST -o OUTFOLDER -s1 CATEGORY1 -s2 CATEGORY2 -p -bn COLLECTIONNAME"
set -e

date

function script_usage() {
    cat << EOF
    __        __                   ____  _
    \ \      / /__  _ __ _ __ ___ | __ )(_) ___  _ __ ___   ___
     \ \ /\ / / _ \| '__| '_ ` _ \|  _ \| |/ _ \| '_ ` _ \ / _ \
      \ V  V / (_) | |  | | | | | | |_) | | (_) | | | | | |  __/
       \_/\_/ \___/|_|  |_| |_| |_|____/|_|\___/|_| |_| |_|\___|

    WormBiom - Pipeline to generate estimated metabolic pathways from 16S
    rRNA biom tables.
    Usage: $USAGE
    Options:
       -h|--help                Display this help

       -a|--ASVtable            Required. Biom/tsv/csv File

       -m|--ASVmeta             Required. Count table related Metadata

       -s|--ASVsequence         Required. related ASV sequences

       -b|--BacteriaList        Required. Collection Name or list of bacteria
                                name used in the experiment. One per line

       -o|--OutFolder           Output OutFolder
                                Default: ./OUT/

       -s1|--Selector1          Name of column to use as category 1
                                used for graph plot categorisation and basic
                                statistics

       -k|--kegg                Generate Kegg annotation table

       -p|--PathwayConvert      Generate Metacyc Metabolic Pathway Table

       -pl|--PathwayTable       If Metabolic Pathway is already generated,
                                indicate the location of the file here
                                Incompatible with -p

       -kl|--keggTable          If Kegg annotation Table is already generated,
                                indicate the location of the file here
                                Incompatible with -k

       -bn|--BacteriaName       Bacteria Collection Name

       -st|--Stats              Do stats on Selector 1

       -v|--Verbose             Verbose mode, the scripts talk to you
EOF
}

#Setting up variable
## General
GMeta=$DIR/Data/Genome.metadata.tsv
SSUCount=$DIR/Data/16S.Counts.txt
SSUSeq=$DIR/Data/16S.sequences.fasta

MetacycDB=$DIR/Data/Metacyc.database.tsv
MetacycMeta=$DIR/Data/Metacyc.metadata.tsv

WB=$DIR/Data/WB.tsv
KeggMeta=$DIR/Data/Kegg.metadata.csv

RunKegg=1
RunMeta=1
RunGFF=1
RunHyp=1
STATS=0

OUT="./WormBiom.Result/"
DEBUG=0

CountThreshold=1000

## Options

WBoption2=NULL
BNAME=$(date +%Y%m%d)

while [ "$1" != "" ]; do
  case $1 in
      -h|--help)
       script_usage
       exit 0
       ;;
     -a|--ASVtable)
       shift
       ASVTable=$1
       ;;
     -m|--ASVmeta)
       shift
       ASVMeta=$1
       ;;
     -s|--ASVsequence)
       shift
       ASVsequence=$1
       ;;
     -b|--BacteriaList)
       shift
       BACList=$1
       ;;
     -o|--OutFolder)
       shift
       OUT=$1
       ;;
     -s1|--Selector1)
       shift
       Sel1=$1
       ;;
     -k|--kegg)
       shift
       RunKegg=$1
       ;;
     -p|--PathwayConvert)
      shift
      RunMeta=$1
      ;;
    -bn|--BacteriaName)
      shift
      BNAME=$1
      ;;
    -v|--Verbose)
      DEBUG=1
        ;;
    -st|--Stats)
      STATS=1
        ;;
      *)
       echo "Invalid command: no parameter included with argument $1"
       exit 0
       ;;
  esac
  shift
done

mkdir -p $OUT
REFMissing=$OUT/Blast.missing.reference.txt
REFsequence=$DIR/Data/Collection/$BACList\.fasta

#Checking for Blast
echo "Looking for Blast"
echo
MKBLASDB=$(which makeblastdb)
TBLASTN=$(which blastn)
if [ -z ${MKBLASDB+x} ]; then echo "makeblastdb has not been found, stopping" && exit 1; else echo "makeblastdb has been found"; fi
if [ -z ${TBLASTN+x} ]; then echo "blastn has not been found, stopping" && exit 1; else echo  "blastn has been found"; fi
echo
echo "----"
#Checking Bacteria list
echo "Checking Collection"
echo
echo $DIR/Data/Collection/$BACList\.fasta

if ls $DIR/Data/Collection/$BACList\.fasta 1> /dev/null 2>&1;
then echo "Found Bacteria $BACList Collection";
  BNAME=$BACList;
else echo "Creating Bacteria Collection";
  REFsequence=$DIR/Data/Collection/$BNAME\.fasta
  [ -e "$REFMissing" ] && rm -f $REFMissing;
  [ -e "$REFsequence" ] && rm -f $REFsequence;
  for i in  $(cat $BACList);
   do a=$(cat $SSUSeq | grep -w -A 1 $i || true);
    if (( $(grep -c . <<<"$a") > 1 ));
      then echo $a >> $REFsequence;
    else echo $i >> $REFMissing;
    fi;
  done;
  echo "New Collection Created"
  report1=$(grep -c "^" $BACList)
  if ls $REFMissing 1> /dev/null 2>&1; then report2=$(grep -c "^" $REFMissing); else report2=0; fi
  echo $report2"/"$report1" were missing in the reference"
fi
echo

sed -i "s/ /\n/g" $REFsequence

echo "----"
#Checking Blast Database
echo "Checking Blast Database"
echo
if ls $DIR/BlastDB/$BNAME 1> /dev/null 2>&1;
  then echo "Found Blast database, using it." ;
else echo "Blast database not found, creating...";
    makeblastdb -dbtype nucl -in $REFsequence -title $BNAME -out $DIR/BlastDB/$BNAME;
fi
echo
echo "----"
#Blasting
echo "Running blast to map ASV to Genomes"
BlastResult=$OUT/Blast.Result.txt
blastn -db $DIR/BlastDB/$BNAME -query $ASVsequence -outfmt 6 -perc_identity 99 -qcov_hsp_perc 100 -out $BlastResult
echo "Blast done"
echo
echo "----"
#Running R script
echo "Running WormBiom"
echo $VERSION
echo
echo "Checking modules to run:"
echo
if [ $RunMeta == 1 ];
  then RunMeta=$(echo "--Metacyc") && echo "Will run Metacyc predictor" && MetacycLoad="Nope";
  else MetacycLoad=$RunMeta && RunMeta=$(echo "--load") && echo "Loading Metacyc table $MetacycLoad";
fi

if [ $RunKegg == 1 ];
  then RunKegg=$(echo "--kegg") && echo "Will run Kegg predictor" && KeggLoad="Nope";
  else KeggLoad=$RunKegg && RunKegg=$(echo "--load");
fi

if [ $RunGFF == 1 ];
  then RunGFF=$(echo "--gff") && echo "Will run Kegg predictor" && GFFLoad="Nope";
  else GFFLoad=$RunGFF && RunGFF=$(echo "--load");
fi

if [ $RunHyp == 1 ];
  then RunHyp=$(echo "--hyp") && echo "Will run Kegg predictor" && HypLoad="Nope";
  else HypLoad=$RunHyp && RunHyp=$(echo "--load");
fi

echo "Checking input count table:"
echo
if [ $(head -n 1 $ASVTable | grep -c "# Constructed from biom file") == 1 ];
  then Tableopt1=$(echo "BIOM") && echo "tsbiom file detected";
elif [ $(cat $ASVTable | awk -F'\t' '{print NF}' | sort -nu | tail -n1) > 1 ] ;
  then Tableopt1=$(echo "TSV") && echo "tsv file detected";
elif [ $(cat $ASVTable | awk -F',' '{print NF}' | sort -nu | tail -n1) > 1 ] ;
  then Tableopt1=$(echo "CSV") && echo "csv biom file detected";
else echo "Not recognizing file type... stopping" && break 0;
  fi

SourceDir=$DIR/R/WB.script.R
echo here: $Tableopt1 $SourceDir $DEBUG $STATS
echo
#line for debug
#echo "Rscript $DIR/R/WB.R $ASVTable	$ASVMeta	$BlastResult	$OUT	$GMeta	$MetacycDB	$MetacycMeta	$KeggMeta	$KeggDB	$Sel1	$RunMeta	$RunKegg	$MetacycLoad	$KeggLoad	$SSUCount	$DEBUG	$STATS	$CountThreshold $Tableopt1 $SourceDir"

Rscript $DIR/R/WB.R $ASVTable	$ASVMeta	$BlastResult	$OUT	$GMeta	$MetacycDB	$MetacycMeta	$KeggMeta	$WB	$Sel1	$RunMeta  $RunKegg  $MetacycLoad  $KeggLoad $SSUCount $DEBUG  $STATS  $CountThreshold $Tableopt1  $SourceDir $RunGFF $RunHyp
echo
echo "----"
#Creating report
#TBA
